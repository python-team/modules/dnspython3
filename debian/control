Source: dnspython3
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Robert S. Edmonds <edmonds@debian.org>, Matthew Grant <matthewgrant5@gmail.com>, Scott Kitterman <scott@kitterman.com>
Build-Depends: debhelper (>= 8.1), python3-all (>= 3.2), dh-python
Homepage: http://www.dnspython.org
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/dnspython3.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/dnspython3

Package: python3-dnspython
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: DNS toolkit for Python (Python 3)
 dnspython is a DNS toolkit for Python. It supports almost all record types. It
 can be used for queries, zone transfers, and dynamic updates. It supports TSIG
 authenticated messages and EDNS0.
 .
 dnspython provides both high and low level access to DNS. The high level
 classes perform queries for data of a given name, type, and class, and return
 an answer set. The low level classes allow direct manipulation of DNS zones,
 messages, names, and records.
 .
 This is the Python 3 version.
